About NetSSH
=============

The goals of this project are to create alternative to `netmiko`_ by replacing `Paramiko` with `ssh-python`_.

As `ssh-python` is designed to be faster than `Paramiko`, so should this library.

This originally started as fork of `netssh2`_ with the change of using ssh-python (and libssh) under the hood.

Contributions are most welcome!

.. _netmiko: https://github.com/ktbyers/netmiko
.. _ssh-python: https://github.com/ParallelSSH/ssh-python
.. _netssh2: https://gitlab.com/jkrysl/netssh2