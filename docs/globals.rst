netssh.globals
===============

.. automodule:: netssh.globals
   :members:
   :undoc-members:
   :member-order: groupwise
