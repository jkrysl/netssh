Installation
============

The recommended installation method is using `pip`.


.. code-block:: shell

    pip install netssh


Installation from source
========================


.. code-block:: shell

    git clone https://gitlab.com/jkrysl/netssh
    cd netssh
    python setup.py install