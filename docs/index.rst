NetSSH Documentation
=====================

 Library for communicating with network devices using `ssh-python`_.

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   introduction
   installation
   api
   changelog_link


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

.. _ssh-python: https://github.com/ParallelSSH/ssh-python
