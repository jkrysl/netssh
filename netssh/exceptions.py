"""
Module for defining netssh exceptions
"""
from __future__ import absolute_import
from ssh.exceptions import AuthenticationError  # pylint: disable=E0611


class NetSshAuthenticationError(AuthenticationError):  # pylint: disable=R0903
    """
    Base exception for any authentication errors
    """


class NetSshTimeout(Exception):  # pylint: disable=R0903
    """
    Base exception for any timeout errors
    """


class NetSshChannelException(Exception):
    """
    Base exception for any channel errors
    """


class NetSshTooManyRetriesException(Exception):
    """
    Base exception when ran out of retries.
    """


class NetSshHostError(Exception):
    """
    Base exception for any host errors
    """
