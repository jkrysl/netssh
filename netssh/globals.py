"""
File for storing global variables
"""

LIBSSH_ERROR_EAGAIN = -37  #: SSH return value of blocked socket
LIBSSH_SESSION_BLOCK_INBOUND = 0x0001  #: SSH flag to block the socket
