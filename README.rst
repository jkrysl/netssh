.. image:: https://img.shields.io/pypi/v/ssh-python.svg?label=ssh-python&style=plastic
   :target: https://badge.fury.io/py/ssh-python
   :alt: ssh-python
.. image:: https://img.shields.io/pypi/v/netssh.svg?style=plastic
   :target: https://pypi.python.org/pypi/ssh-python
   :alt: PyPI
.. image:: https://img.shields.io/pypi/l/netssh.svg?label=License&style=plastic
   :alt: PyPI - License
   :target: https://gitlab.com/jkrysl/netssh/blob/master/LICENSE
.. image:: https://img.shields.io/pypi/pyversions/ssh-python.svg?style=plastic
   :target: https://pypi.python.org/pypi/ssh-python
.. image:: https://img.shields.io/pypi/wheel/netssh.svg?style=plastic
   :alt: PyPI - Wheel
   :target: https://pypi.python.org/pypi/ssh-python
.. image:: https://img.shields.io/gitlab/pipeline/jkrysl/netssh.svg?style=plastic
   :alt: Gitlab pipeline status
   :target: https://badge.fury.io/py/ssh-python
.. image:: https://img.shields.io/readthedocs/netssh.svg?style=plastic
   :alt: Read the Docs
   :target: https://netssh.readthedocs.io/en/latest/

Project description
===================

Library to use ssh-python to communicate with different network devices.

Installation
============

    pip install netssh


Documentation
=============
Documentation can be found here: https://netssh.readthedocs.io/en/latest/
